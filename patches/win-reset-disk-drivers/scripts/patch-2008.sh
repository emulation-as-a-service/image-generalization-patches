#!/bin/sh

set -eu

# Patching Widows system registry, as described in:
# https://blog.mcbachmann.de/linux/windows-bluescreen-0x0000007b-inaccessible_boot_device

__patch_content() {
	# Patch's content comes from:
	# https://blog.mcbachmann.de/wp-content/uploads/2009/06/mergeide.chntpw
	cat << '__PATCH_CONTENT_END__'
cd \ControlSet001\Control\CriticalDeviceDatabase
nk Primary_IDE_Channel
cd Primary_IDE_Channel
nv 1 ClassGUID
ed ClassGUID
{4D36E96A-E325-11CE-BFC1-08002BE10318}
nv 1 Service
ed Service
atapi

cd \ControlSet001\Control\CriticalDeviceDatabase
nk Secondary_IDE_Channel
cd Secondary_IDE_Channel
nv 1 ClassGUID
ed ClassGUID
{4D36E96A-E325-11CE-BFC1-08002BE10318}
nv 1 Service
ed Service
atapi

cd \ControlSet001\Control\CriticalDeviceDatabase
nk *pnp0600
cd *pnp0600
nv 1 ClassGUID
ed ClassGUID
{4D36E96A-E325-11CE-BFC1-08002BE10318}
nv 1 Service
ed Service
atapi

cd \ControlSet001\Control\CriticalDeviceDatabase
nk *azt0502
cd *azt0502
nv 1 ClassGUID
ed ClassGUID
{4D36E96A-E325-11CE-BFC1-08002BE10318}
nv 1 Service
ed Service
atapi

cd \ControlSet001\Control\CriticalDeviceDatabase
nk GenDisk
cd GenDisk
nv 1 ClassGUID
ed ClassGUID
{4D36E967-E325-11CE-BFC1-08002BE10318}
nv 1 Service
ed Service
disk

cd \ControlSet001\Control\CriticalDeviceDatabase
nk PCI#CC_0101
cd PCI#CC_0101
nv 1 ClassGUID
ed ClassGUID
{4D36E96A-E325-11CE-BFC1-08002BE10318}
nv 1 Service
ed Service
pciide

cd \ControlSet001\Control\CriticalDeviceDatabase
nk PCI#VEN_0E11&DEV_AE33
cd PCI#VEN_0E11&DEV_AE33
nv 1 ClassGUID
ed ClassGUID
{4D36E96A-E325-11CE-BFC1-08002BE10318}
nv 1 Service
ed Service
pciide

cd \ControlSet001\Control\CriticalDeviceDatabase
nk PCI#VEN_1039&DEV_0601
cd PCI#VEN_1039&DEV_0601
nv 1 ClassGUID
ed ClassGUID
{4D36E96A-E325-11CE-BFC1-08002BE10318}
nv 1 Service
ed Service
pciide

cd \ControlSet001\Control\CriticalDeviceDatabase
nk PCI#VEN_1039&DEV_5513
cd PCI#VEN_1039&DEV_5513
nv 1 ClassGUID
ed ClassGUID
{4D36E96A-E325-11CE-BFC1-08002BE10318}
nv 1 Service
ed Service
pciide

cd \ControlSet001\Control\CriticalDeviceDatabase
nk PCI#VEN_1042&DEV_1000
cd PCI#VEN_1042&DEV_1000
nv 1 ClassGUID
ed ClassGUID
{4D36E96A-E325-11CE-BFC1-08002BE10318}
nv 1 Service
ed Service
pciide

cd \ControlSet001\Control\CriticalDeviceDatabase
nk PCI#VEN_105A&DEV_4D33
cd PCI#VEN_105A&DEV_4D33
nv 1 ClassGUID
ed ClassGUID
{4D36E96A-E325-11CE-BFC1-08002BE10318}
nv 1 Service
ed Service
pciide

cd \ControlSet001\Control\CriticalDeviceDatabase
nk PCI#VEN_1095&DEV_0640
cd PCI#VEN_1095&DEV_0640
nv 1 ClassGUID
ed ClassGUID
{4D36E96A-E325-11CE-BFC1-08002BE10318}
nv 1 Service
ed Service
pciide

cd \ControlSet001\Control\CriticalDeviceDatabase
nk PCI#VEN_1095&DEV_0646
cd PCI#VEN_1095&DEV_0646
nv 1 ClassGUID
ed ClassGUID
{4D36E96A-E325-11CE-BFC1-08002BE10318}
nv 1 Service
ed Service
pciide

cd \ControlSet001\Control\CriticalDeviceDatabase
nk PCI#VEN_1095&DEV_0646&REV_05
cd PCI#VEN_1095&DEV_0646&REV_05
nv 1 ClassGUID
ed ClassGUID
{4D36E96A-E325-11CE-BFC1-08002BE10318}
nv 1 Service
ed Service
pciide

cd \ControlSet001\Control\CriticalDeviceDatabase
nk PCI#VEN_1095&DEV_0646&REV_07
cd PCI#VEN_1095&DEV_0646&REV_07
nv 1 ClassGUID
ed ClassGUID
{4D36E96A-E325-11CE-BFC1-08002BE10318}
nv 1 Service
ed Service
pciide

cd \ControlSet001\Control\CriticalDeviceDatabase
nk PCI#VEN_1095&DEV_0648
cd PCI#VEN_1095&DEV_0648
nv 1 ClassGUID
ed ClassGUID
{4D36E96A-E325-11CE-BFC1-08002BE10318}
nv 1 Service
ed Service
pciide

cd \ControlSet001\Control\CriticalDeviceDatabase
nk PCI#VEN_1095&DEV_0649
cd PCI#VEN_1095&DEV_0649
nv 1 ClassGUID
ed ClassGUID
{4D36E96A-E325-11CE-BFC1-08002BE10318}
nv 1 Service
ed Service
pciide

cd \ControlSet001\Control\CriticalDeviceDatabase
nk PCI#VEN_1097&DEV_0038
cd PCI#VEN_1097&DEV_0038
nv 1 ClassGUID
ed ClassGUID
{4D36E96A-E325-11CE-BFC1-08002BE10318}
nv 1 Service
ed Service
pciide

cd \ControlSet001\Control\CriticalDeviceDatabase
nk PCI#VEN_10AD&DEV_0001
cd PCI#VEN_10AD&DEV_0001
nv 1 ClassGUID
ed ClassGUID
{4D36E96A-E325-11CE-BFC1-08002BE10318}
nv 1 Service
ed Service
pciide

cd \ControlSet001\Control\CriticalDeviceDatabase
nk PCI#VEN_10AD&DEV_0150
cd PCI#VEN_10AD&DEV_0150
nv 1 ClassGUID
ed ClassGUID
{4D36E96A-E325-11CE-BFC1-08002BE10318}
nv 1 Service
ed Service
pciide

cd \ControlSet001\Control\CriticalDeviceDatabase
nk PCI#VEN_10B9&DEV_5215
cd PCI#VEN_10B9&DEV_5215
nv 1 ClassGUID
ed ClassGUID
{4D36E96A-E325-11CE-BFC1-08002BE10318}
nv 1 Service
ed Service
pciide

cd \ControlSet001\Control\CriticalDeviceDatabase
nk PCI#VEN_10B9&DEV_5219
cd PCI#VEN_10B9&DEV_5219
nv 1 ClassGUID
ed ClassGUID
{4D36E96A-E325-11CE-BFC1-08002BE10318}
nv 1 Service
ed Service
pciide

cd \ControlSet001\Control\CriticalDeviceDatabase
nk PCI#VEN_10B9&DEV_5229
cd PCI#VEN_10B9&DEV_5229
nv 1 ClassGUID
ed ClassGUID
{4D36E96A-E325-11CE-BFC1-08002BE10318}
nv 1 Service
ed Service
pciide

cd \ControlSet001\Control\CriticalDeviceDatabase
nk PCI#VEN_1106&DEV_0571
cd PCI#VEN_1106&DEV_0571
nv 1 Service
ed Service
pciide
nv 1 ClassGUID
ed ClassGUID
{4D36E96A-E325-11CE-BFC1-08002BE10318}

cd \ControlSet001\Control\CriticalDeviceDatabase
nk PCI#VEN_8086&DEV_1222
cd PCI#VEN_8086&DEV_1222
nv 1 ClassGUID
ed ClassGUID
{4D36E96A-E325-11CE-BFC1-08002BE10318}
nv 1 Service
ed Service
intelide

cd \ControlSet001\Control\CriticalDeviceDatabase
nk PCI#VEN_8086&DEV_1230
cd PCI#VEN_8086&DEV_1230
nv 1 ClassGUID
ed ClassGUID
{4D36E96A-E325-11CE-BFC1-08002BE10318}
nv 1 Service
ed Service
intelide

cd \ControlSet001\Control\CriticalDeviceDatabase
nk PCI#VEN_8086&DEV_2411
cd PCI#VEN_8086&DEV_2411
nv 1 ClassGUID
ed ClassGUID
{4D36E96A-E325-11CE-BFC1-08002BE10318}
nv 1 Service
ed Service
intelide

cd \ControlSet001\Control\CriticalDeviceDatabase
nk PCI#VEN_8086&DEV_2421
cd PCI#VEN_8086&DEV_2421
nv 1 ClassGUID
ed ClassGUID
{4D36E96A-E325-11CE-BFC1-08002BE10318}
nv 1 Service
ed Service
intelide

cd \ControlSet001\Control\CriticalDeviceDatabase
nk PCI#VEN_8086&DEV_7010
cd PCI#VEN_8086&DEV_7010
nv 1 ClassGUID
ed ClassGUID
{4D36E96A-E325-11CE-BFC1-08002BE10318}
nv 1 Service
ed Service
intelide

cd \ControlSet001\Control\CriticalDeviceDatabase
nk PCI#VEN_8086&DEV_7111
cd PCI#VEN_8086&DEV_7111
nv 1 ClassGUID
ed ClassGUID
{4D36E96A-E325-11CE-BFC1-08002BE10318}
nv 1 Service
ed Service
intelide

cd \ControlSet001\Control\CriticalDeviceDatabase
nk PCI#VEN_8086&DEV_7199
cd PCI#VEN_8086&DEV_7199
nv 1 ClassGUID
ed ClassGUID
{4D36E96A-E325-11CE-BFC1-08002BE10318}
nv 1 Service
ed Service
intelide


cd \ControlSet001\services
nk atapi
cd atapi
nv 4 ErrorControl
ed ErrorControl
1
nv 1 Group
ed Group
SCSI miniport
nv 4 Start
ed Start
0
nv 4 Tag
ed Tag
19
nv 4 Type
ed Type
1
nv 1 DisplayName
ed DisplayName
Standard IDE/ESDI Hard Disk Controller
nv 2 ImagePath
ed ImagePath
system32\DRIVERS\atapi.sys


cd \ControlSet001\services
nk intelide
cd intelide
nv 4 ErrorControl
ed ErrorControl
1
nv 1 Group
ed Group
System Bus Extender
nv 4 Start
ed Start
0
nv 4 Tag
ed Tag
4
nv 4 Type
ed Type
1
nv 2 ImagePath
ed ImagePath
System32\DRIVERS\intelide.sys


cd \ControlSet001\services
nk pciide
cd pciide
nv 4 ErrorControl
ed ErrorControl
1
nv 1 Group
ed Group
System Bus Extender
nv 4 Start
ed Start
0
nv 4 Tag
ed Tag
3
nv 4 Type
ed Type
1
nv 2 ImagePath
ed ImagePath
System32\DRIVERS\pciide.sys

q
y
__PATCH_CONTENT_END__
}

__info() {
	echo "[INFO] $1"
}

__error() {
	echo "[ERROR] $1"
}

__fail() {
	echo "Aborting..."
	exit 1
}


# ===== PATCH APPLICATION ====================

mountpoint="$1"

if [ -z "${mountpoint}" ]; then
	__error 'No mountpoint specified!'
	__fail
fi

workdir="$(mktemp -d --tmpdir image-generalization-XXXXXXXX)"

__cleanup() {
	__info "Cleaning up..."
	rm -r -v "${workdir}"
}

trap __cleanup EXIT

__info "Searching for system registry..."
sysreg="$(find "${mountpoint}" -type f -iwholename '*/windows/system32/config/system' | head -n 1)"
__info "Found system registry: ${sysreg}"

__info "Backing up system registry..."
cp -v "${sysreg}" "${sysreg}.bak"

__info "Looking up current control-set number..."
output="${workdir}/output"
cat <<EOT | chntpw -e "${sysreg}"
ek ${output} System Select
q
EOT

# get last 3 digits of the field 'current'
curnum="$(grep -i current "${output}" | cut -f2 -d ':' | tr -d '\r\n' | tail -c 3)"
ctlset="ControlSet${curnum}"
__info "Using ${ctlset} as current-control-set..."
rm -v "${output}"

__info "Patching system registry..."
# NOTE: chntpw returns 2 when registry was written and 0 when not.
#       Everything else will be considered an error!
__patch_content | sed "s/ControlSet001/${ctlset}/" \
	| chntpw -e "${sysreg}" || test "$?" = '2'

__info "Patching done!"
